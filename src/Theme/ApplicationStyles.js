import { Dimensions } from "react-native";

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; 
var primaryColor = '#045e9f';

const ApplicationStyles = {
    container: {
        flex: 1,
        backgroundColor: '#f8f8f8',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        flexDirection: 'row'
    },
    contentContainer: {
        flex: 1,
        margin: 10,
        marginBottom: 0
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.1,
        shadowRadius: 1.5,
        elevation: 3
    },
    row: {
        marginVertical: 5,
    },
    mainLoader: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    card2D: {
        marginVertical: 5,
        padding: 5,
        minHeight: 20,
        backgroundColor: "#fff",
    },
    card: {
        marginVertical: 5,
        padding: 5,
        borderWidth: 1,
        borderRadius: 2,
        borderColor: "#ccc",
        flexWrap: "wrap",
        backgroundColor: "#fff",
        elevation: 1
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.1,
        shadowRadius: 1.5,
        elevation: 3
    },
    flex05: {
        flex: 0.5
    },
    flexColumn: {
        flexDirection: 'column'
    },
    flexRow: {
        flexDirection: 'row'
    },
    textCenter: {
        textAlign: 'center'
    },
    h1: {
        fontSize: 36
    },
    h2: {
        fontSize: 34
    },
    h3: {
        fontSize: 30
    },
    h4: {
        fontSize: 28
    },
    h5: {
        fontSize: 22
    },
    h6: {
        fontSize: 16
    },
    mv10: {
        marginVertical: 10
    },
    mr5: {
        marginRight: 5
    },
    pl10: {
        paddingLeft: 10
    },
    bb1: {
        borderBottomWidth: 1,
        borderColor: primaryColor,
    },
    br1: {
        borderRightWidth: 1,
        borderColor: primaryColor,
    },
    label: {
        fontSize: 12,
        color: '#808080'
    },
    smallText: {
        fontSize: 10
    },
    btn: {
        borderWidth: 1,
        borderRadius: 2,
        borderColor: primaryColor,
        flexWrap: "wrap",
        paddingVertical: 10,
        paddingHorizontal: 15
    },
    activeBtn: {
        backgroundColor: primaryColor,
    },
    activeText: {
        color: '#fff'
    },
    seperater: {
        borderBottomWidth: 1,
        borderColor: "#ccc",
    },
    primaryColor: {
        color: primaryColor
    },
    bgPrimary: {
        backgroundColor: primaryColor
    },
    contentWrap: {
        flexWrap: 'wrap'
    },
}

export default ApplicationStyles