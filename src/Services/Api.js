import apisauce from 'apisauce'

const create = (baseURL = 'https://n2fs1jsi01.execute-api.us-east-1.amazonaws.com/', timeout = 10000) => {
    const api = apisauce.create({
        baseURL,
        headers: {
          'Cache-Control': 'no-cache'
        },
        timeout
      })

    // Get Requests
    const getProjects = () => api.get('prototype/projects/', {})
    const getProjectsByID = (pID) => api.get('prototype/projects/'+pID, {})
    const getProjectsByItemID = (pID, itemID) => api.get('prototype/projects/' + pID + '/items/' + itemID, {})

    // Post Requests
    const postLogin = (username, password) => api.post('prototype/auth/login', {username: username, password: password})
    const postForgotPassword = (email) => api.post('prototype/auth/forgotpassword', {email: email})
    const postRestPassword = (currentPassword, newPassword) => api.post('prototype/auth/forgotpassword', {currentPassword: currentPassword, newPassword: newPassword})

    return {
        getProjects,
        getProjectsByID,
        getProjectsByItemID,
        postLogin,
        postForgotPassword,
        postRestPassword
    }
}

export default {
    create
}