export default {
    // Functions return fixtures
    getHomeScreenData: () => {
      return {
        ok: true,
        data: require('../Fixtures/home.json')
      }
    },
    getProjectDetailScreenData: () => {
      return {
        ok: true,
        data: require('../Fixtures/projectDetail.json')
      }
    },
    getItemDetailScreenData: () => {
      return {
        ok: true,
        data: require('../Fixtures/itemDetail.json')
      }
    }
  }
  