import { createStackNavigator } from 'react-navigation';
import React from 'react';
import { Image } from 'react-native';

import Home from '../Containers/HomeScreen'
import ProjectDetails from '../Containers/ProjectDetailsScreen'
import ItemDetails from '../Containers/ItemDetailsScreen'

const RootStack = createStackNavigator({
        Home: {
            screen: Home,
            navigationOptions: {
                headerLeft: (
                    <Image resizeMode={'cover'} style={{width: 40, height: 40}} source={require('../Images/menu.png')}/>
                ),
                headerTitle: (
                    <Image resizeMode={'cover'} style={{width: 40, height: 40}} source={require('../Images/logo.png')}/>
                ),
                headerStyle: {
                    backgroundColor: '#045e9f',
                }
            }
        },
        ProjectDetails: {
            screen: ProjectDetails,
            navigationOptions: {
                headerStyle: {
                    backgroundColor: '#045e9f'
                },
                headerTintColor: '#ffffff'
            }
        },
        ItemDetails: {
            screen: ItemDetails,
            navigationOptions: {
                headerStyle: {
                    backgroundColor: '#045e9f'
                },
                headerTintColor: '#ffffff'
            }
        }
    },
    {
        initialRouteName: 'Home'
    }
);
  
  export default RootStack;