import React from 'react';
import { StyleSheet, Text, View, ScrollView, Image, TouchableOpacity, ActivityIndicator } from 'react-native';

import FixtureAPI from '../Services/FixtureAPI';
import ApplicationStyles from '../Theme/ApplicationStyles'
import API from '../Services/Api'

export default class ItemDetailsScreen extends React.Component {

    constructor(props) {
        super(props)
        this.api = API.create()
        const { navigation } = this.props;
        this.projectId = navigation.getParam('projectID', '');
        this.state = {
            data: null
        }
        this.onResponse = this.onResponse.bind(this)
    }

    static navigationOptions = ({navigation}) => {
        return {
            title: 'Project # ' + navigation.getParam('projectID', '')
        }
    };

    componentDidMount() {
        let projectId = 1,
            itemId = 1
        this.api.getProjectsByItemID
            .apply(this, [projectId, itemId])
            .then( (results) => {
                this.onResponse(results)
            }
        )
    }

    onResponse(response) {
        if(response.status == 200) {
            var responseData = response.data
            this.setState({data: responseData})
        }
    }    

    render() {
        if(this.state.data == null) {
            return (
                <View style={styles.mainLoader}>
                    <ActivityIndicator
                        size={'large'}
                        color={"#045e9f"}
                        animating={true} />
                </View>
            )
        }
        else {
            let projects_details = this.state.data.projects_details
            let items_profile = this.state.data.items_profile
            let item_documents = this.state.data.item_documents
            return (
                <View style={styles.container}>
                    <ScrollView>
                        <View style={{height: 250}}>
                            <Image style={{width: undefined, height: undefined, flex:1}} source={require('../Images/steel-pipe-products.jpg')}/>
                        </View>
                        <View style={[styles.contentContainer]}>
                            <View style={[styles.card, styles.projectDetailSection]}>
                                <View style={[styles.flexRow, styles.headerRow1]}>
                                    <Text style={styles.highlight}>ID #{projects_details.itemID}</Text>
                                    <Text style={[styles.label, styles.primaryColor]}>{`${projects_details.category} | ${projects_details.type_of_pipe}`}</Text>
                                </View>
                                <View style={styles.mv10}>
                                    <Text style={styles.title} >{projects_details.title}</Text>
                                    <Text style={styles.desc} >{`Delivery Location : ${projects_details.location}`}</Text>
                                </View>
                                <View style={styles.seperater}></View>
                                <View style={[styles.flexRow, styles.headFooter]}>
                                    <View >
                                        <Text style={[styles.label, styles.smallText, styles.textCenter]}>QUANTITY AVAILABLE</Text>
                                        <Text style={[styles.textCenter]}>{projects_details.quantity_available}</Text>
                                    </View>
                                    <View>
                                        <Text style={[styles.label, styles.smallText, styles.textCenter]}>QUANTITY WANTED</Text>
                                        <Text style={[styles.textCenter]}>{projects_details.quantity_wanted}</Text>
                                    </View>
                                    <View>
                                        <Text style={[styles.label, styles.smallText, styles.textCenter]}>ORIGINAL PRICE</Text>
                                        <Text style={[styles.textCenter]}>{`${projects_details.original_price_by_ft} | ${projects_details.original_price_by_st}`}</Text>
                                    </View>
                                </View>
                                <View style={[styles.flexRow, styles.headFooter2]}>
                                    <View >
                                        <Text style={[styles.label, styles.smallText, styles.textCenter]}>ASKING PRICE</Text>
                                        <Text style={[styles.textCenter]}>{`${projects_details.asking_price_by_ft} | ${projects_details.asking_price_by_st}`}</Text>
                                    </View>
                                    <View>
                                        <Text style={[styles.label, styles.smallText, styles.textCenter]}>QUOTED PRICE</Text>
                                        <Text style={[styles.textCenter]}>{`${projects_details.quoted_price_by_ft} | ${projects_details.quoted_price_by_st}`}</Text>
                                    </View>
                                </View>
                                <View style={[styles.mv10, styles.descriptionContent]}>
                                    <Text numberOfLines = { 4 }style={styles.desc} >{projects_details.description}</Text>
                                    <TouchableOpacity onPress={()=>{}} style={[styles.readMore]}>
                                        <Text style={styles.primaryColor}>{'READ MORE'}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>

                            <View style={[styles.card, styles.noMargin]}>
                                <View style={[styles.cardHeader, styles.bgPrimary, styles.flexRow]}>
                                    <View style={styles.flex05}>
                                        <Text style={[styles.headerTxt, styles.textCenter]}>{'Configurations'}</Text>
                                    </View>
                                    <View style={styles.flex05}>
                                        <Text style={[styles.headerTxt, styles.textCenter]}>{'Details'}</Text>
                                    </View>
                                </View>
                                <View style={[styles.cardContent]}>
                                    <View style={[styles.flexRow]}>
                                        <View style={[styles.flex05, styles.bb1, styles.br1, styles.td]}>
                                            <Text style={[styles.tdTxt]}>{'Grade'}</Text>
                                        </View>
                                        <View style={[styles.flex05, styles.bb1, styles.td]}>
                                            <Text style={[styles.tdTxt]}>{items_profile.grade}</Text>
                                        </View>
                                    </View>
                                    <View style={[styles.flexRow]}>
                                        <View style={[styles.flex05, styles.bb1, styles.br1, styles.td]}>
                                            <Text style={[styles.tdTxt]}>{'OD (in)'}</Text>
                                        </View>
                                        <View style={[styles.flex05, styles.bb1, styles.td]}>
                                            <Text style={[styles.tdTxt]}>{items_profile.od}</Text>
                                        </View>
                                    </View>
                                    <View style={[styles.flexRow]}>
                                        <View style={[styles.flex05, styles.bb1, styles.br1, styles.td]}>
                                            <Text style={[styles.tdTxt]}>{'Wall Thickness'}</Text>
                                        </View>
                                        <View style={[styles.flex05, styles.bb1, styles.td]}>
                                            <Text style={[styles.tdTxt]}>{items_profile.wall_thickness}</Text>
                                        </View>
                                    </View>
                                    <View style={[styles.flexRow]}>
                                        <View style={[styles.flex05, styles.bb1, styles.br1, styles.td]}>
                                            <Text style={[styles.tdTxt]}>{'Coating'}</Text>
                                        </View>
                                        <View style={[styles.flex05, styles.bb1, styles.td]}>
                                            <Text style={[styles.tdTxt]}>{items_profile.coating}</Text>
                                        </View>
                                    </View>
                                    <View style={[styles.flexRow]}>
                                        <View style={[styles.flex05, styles.bb1, styles.br1, styles.td]}>
                                            <Text style={[styles.tdTxt]}>{'Seam Type'}</Text>
                                        </View>
                                        <View style={[styles.flex05, styles.bb1, styles.td]}>
                                            <Text style={[styles.tdTxt]}>{items_profile.seam_type}</Text>
                                        </View>
                                    </View>
                                    <View style={[styles.flexRow]}>
                                        <View style={[styles.flex05, styles.bb1, styles.br1, styles.td]}>
                                            <Text style={[styles.tdTxt]}>{'Condition'}</Text>
                                        </View>
                                        <View style={[styles.flex05, styles.bb1, styles.td]}>
                                        <Text style={[styles.tdTxt]}>{items_profile.condition}</Text>
                                        </View>
                                    </View>
                                    <View style={[styles.flexRow]}>
                                        <View style={[styles.flex05, styles.bb1, styles.br1, styles.td]}>
                                            <Text style={[styles.tdTxt]}>{'Certifications'}</Text>
                                        </View>
                                        <View style={[styles.flex05, styles.bb1, styles.td]}>
                                            <Text style={[styles.tdTxt]}>{items_profile.cert}</Text>
                                        </View>
                                    </View>
                                    <View style={[styles.flexRow]}>
                                        <View style={[styles.flex05, styles.bb1, styles.br1, styles.td]}>
                                            <Text style={[styles.tdTxt]}>{'End Connection'}</Text>
                                        </View>
                                        <View style={[styles.flex05, styles.bb1, styles.td]}>
                                            <Text style={[styles.tdTxt]}>{items_profile.end_conn}</Text>
                                        </View>
                                    </View>
                                    <View style={[styles.flexRow]}>
                                        <View style={[styles.flex05, styles.bb1, styles.br1, styles.td]}>
                                            <Text style={[styles.tdTxt]}>{'Refurbidhed'}</Text>
                                        </View>
                                        <View style={[styles.flex05, styles.bb1, styles.td]}>
                                            <Text style={[styles.tdTxt]}>{items_profile.refur}</Text>
                                        </View>
                                    </View>
                                    <View style={[styles.flexRow]}>
                                        <View style={[styles.flex05, styles.bb1, styles.br1, styles.td]}>
                                            <Text style={[styles.tdTxt]}>{'Manufaturer'}</Text>
                                        </View>
                                        <View style={[styles.flex05, styles.bb1, styles.td]}>
                                            <Text style={[styles.tdTxt]}>{items_profile.manuf}</Text>
                                        </View>
                                    </View>
                                    <View style={[styles.flexRow]}>
                                        <View style={[styles.flex05, styles.bb1, styles.br1, styles.td]}>
                                            <Text style={[styles.tdTxt]}>{'Manufactured Year'}</Text>
                                        </View>
                                        <View style={[styles.flex05, styles.bb1, styles.td]}>
                                            <Text style={[styles.tdTxt]}>{items_profile.manuf_year}</Text>
                                        </View>
                                    </View>
                                    <View style={[styles.flexRow]}>
                                        <View style={[styles.flex05, styles.bb1, styles.br1, styles.td]}>
                                            <Text style={[styles.tdTxt]}>{'Pounds Per Foot'}</Text>
                                        </View>
                                        <View style={[styles.flex05, styles.bb1, styles.td]}>
                                            <Text style={[styles.tdTxt]}>{items_profile.pounds_per_foot}</Text>
                                        </View>
                                    </View>
                                    <View style={[styles.flexRow]}>
                                        <View style={[styles.flex05, styles.bb1, styles.br1, styles.td]}>
                                            <Text style={[styles.tdTxt]}>{'Photos'}</Text>
                                        </View>
                                        <View style={[styles.flex05, styles.bb1, styles.td]}>
                                            <Text style={[styles.tdTxt]}>{items_profile.photos}</Text>
                                        </View>
                                    </View>
                                    <View style={[styles.flexRow]}>
                                        <View style={[styles.flex05, styles.bb1, styles.br1, styles.td]}>
                                            <Text style={[styles.tdTxt]}>{'MTR'}</Text>
                                        </View>
                                        <View style={[styles.flex05, styles.bb1, styles.td]}>
                                            <Text style={[styles.tdTxt]}>{items_profile.mtr}</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>

                            <View style={[styles.card]}>
                                <View style={[styles.cardHeader, styles.bb1]}>
                                    <Text style={[styles.headerTxt, styles.primaryColor, styles.h5]}>{'Documents'}</Text>
                                </View>
                                <View style={[styles.cardContent, styles.mv10]}>
                                    {
                                        item_documents.map((document, key) => {
                                            return(
                                                <View key={`document-${key}`} style={[styles.flexRow, styles.docSecRow]}>
                                                    <View style={[styles.flex01, styles.mr5]}>
                                                        <Image style={{width: 32, height: 32}} source={require('../Images/doc-icon.png')}/>
                                                    </View>
                                                    <View style={[styles.flex09]}>
                                                        <Text style={[styles.docName]}>{document.name}</Text>
                                                        <Text style={[styles.label]}>{document.size}</Text>
                                                    </View>
                                                </View>
                                            )
                                        })
                                    }
                                </View>
                            </View>        
                        </View>
                    </ScrollView>
                </View>
            )
        }
    }
}


const styles = StyleSheet.create({
    ...ApplicationStyles,
    projectDetailSection: {
        marginTop: -40, 
        backgroundColor: '#fff'
    },
    highlight: {
        color: '#fff',
        paddingVertical: 5,
        paddingHorizontal: 15,
        backgroundColor: "#045e9f",
        fontSize: 12
    },
    itemId: {
        alignSelf: 'flex-start',
        color: '#fff',
        paddingVertical: 5,
        paddingHorizontal: 10,
        backgroundColor: "#045e9f",
        fontSize: 12
    },
    headerRow1: {
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    title: {
        fontSize: 18,
    },
    desc: {
        fontSize: 11,
        color: "#aaa",
        alignSelf: 'center'
    },
    cardContent: {
        alignItems: 'flex-start'
    },
    itemDetails: {
        // alignItems: 'flex-start',
    },
    headFooter: {
        justifyContent: 'space-between',
        paddingVertical: 10,
        alignItems: 'center',
        alignContent: 'center'
    },
    headFooter2: {
        justifyContent: 'space-evenly',
    },
    readMore: {
        alignSelf: 'center'
    },
    descriptionContent: {
        alignContent: 'center',
        justifyContent: 'center',
        alignItems: 'center'
    },
    cardHeader: {
        paddingHorizontal: 10,
        paddingVertical: 15,
        borderTopStartRadius: 2,
        borderTopEndRadius: 2,
    },
    headerTxt:{
        fontSize: 18,
        color: '#fff'
    },
    noMargin: {
        margin: 0,
        padding: 0
    },
    td: {
        paddingHorizontal: 10,
        paddingVertical: 20,
    },
    tdTxt: {
        fontSize: 18
    },
    docSecRow: {
        paddingHorizontal: 10,
        paddingVertical: 10,
    },
    docName: {
        fontSize:14
    }
});