import React from 'react';
import { StyleSheet, Text, View, ScrollView, Image, TouchableOpacity, ActivityIndicator } from 'react-native';

import FixtureAPI from '../Services/FixtureAPI';
import ApplicationStyles from '../Theme/ApplicationStyles'
import API from '../Services/Api'

export default class HomeScreen extends React.Component {
    constructor(props) {
        super(props)
        this.api = API.create()
        this.state = {
            openProjectsList: false,
            data: null,
            totalProjects: '',
            openProject: '',
            closeProject: ''
        }
        this.onResponse = this.onResponse.bind(this)
    }

    componentDidMount() {
        this.api.getProjects
            .apply(this, [])
            .then( (results) => {
                this.onResponse(results)
            }
        )
    }

    projectStateChange(type) {
        switch(type) {
            case 'open': this.setState({openProjectsList: true}); break; 
            case 'close': this.setState({openProjectsList: false}); break; 
        }
    }

    onResponse(response) {
        if(response.status == 200) {
            var counts = response.data.projects_count
            this.setState({
                data: response.data, 
                totalProjects: counts.total_projects, 
                openProject: counts.open_projects, 
                closeProject: counts.close_projects
            })
        }
    }

    render() {
        let activeOpen = null,
            activeTextOpen = null,
            activeClose = null,
            activeTextClose = null;
        
            if(this.state.openProjectsList) {
                activeOpen = styles.activeBtn
                activeTextOpen = styles.activeText
            } else {
                activeClose = styles.activeBtn
                activeTextClose = styles.activeText
            }
        return (
            <View style={styles.container}>
                <ScrollView>
                    <View style={styles.contentContainer} >

                        <View style={styles.card2D}>
                            <View style={[styles.flexRow]}>
                                <View>
                                    <Image resizeMode={'center'} style={{width: 50, height: 50}} source={require('../Images/CristianRoundProfile.png')}/>
                                </View>
                                <View style={styles.pl10}>
                                    <Text>Good Morning !</Text>
                                    <Text style={styles.h5}>Mr. James Anderson</Text>
                                </View>
                            </View>
                        </View>

                        <View style={styles.card2D}>
                            <View style={[styles.flexRow, styles.projectCounts]}>
                                <View>
                                    <Text style={styles.label}>TOTAL PROJECTS</Text>
                                    <Text style={[styles.textCenter, styles.h4]}>{this.state.totalProjects}</Text>
                                </View>
                                <View>
                                    <Text style={styles.label}>OPEN PROJECTS</Text>
                                    <Text style={[styles.textCenter, styles.h4]}>{this.state.openProject}</Text>
                                </View>
                                <View>
                                    <Text style={styles.label}>CLOSE PROJECTS</Text>
                                    <Text style={[styles.textCenter, styles.h4]}>{this.state.closeProject}</Text>
                                </View>
                            </View>
                        </View>

                        <View style={[styles.row, styles.flexRow]}>
                            <TouchableOpacity 
                                style={[styles.btn, styles.flex05, activeOpen]}
                                onPress={this.projectStateChange.bind(this, 'open')}
                            >
                                <Text style={[styles.textCenter, activeTextOpen]}>OPEN PROJECTS</Text>
                            </TouchableOpacity>
                            <TouchableOpacity 
                                style={[styles.btn, styles.flex05, activeClose]}
                                onPress={this.projectStateChange.bind(this, 'close')}
                            >
                                <Text style={[styles.textCenter, activeTextClose]}>CLOSE PROJECTS</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={[styles.row]}>
                            {
                                this.state.data == null ?
                                (
                                    <View style={styles.mainLoader}>
                                        <ActivityIndicator
                                            size={'large'}
                                            color={"#045e9f"}
                                            animating={true} />
                                    </View>
                                ) :
                                (
                                    this.state.openProjectsList ? 
                                    <ProjectList listType={'open'} data={this.state.data.projects_open} navigation={this.props.navigation} /> 
                                    : 
                                    <ProjectList listType={'close'} data={this.state.data.projects_close} navigation={this.props.navigation} />
                                )
                            }
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

class ProjectList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            listType: this.props.listType,
            data: this.props.data
        }
    }
    componentWillReceiveProps(newProps) {
        if(newProps != undefined && newProps != null) {
            this.setState({
                listType: newProps.listType,
                data: newProps.data
            })
        }
    }
    openProjectDetails(pId) {
        this.props.navigation.navigate('ProjectDetails', {
            pID: pId
        });
    }

    render() {
        let projectList = this.state.data
        if(this.state.listType == 'open') {
            return (
                projectList.map((project, key) => {
                    return (
                        <View key={'openProject-'+key} style={styles.card}>
                            <View>
                                <View style={[styles.flexRow, styles.listHeader]}>
                                    <Text style={styles.highlight}>ID #{project.id}</Text>
                                    <Text style={styles.label}>{'Stated on '+project.started_date}</Text>
                                </View>
                                <TouchableOpacity 
                                    style={styles.mv10}
                                    onPress={this.openProjectDetails.bind(this, project.id)}
                                >
                                    <Text style={styles.title} >{project.title}</Text>
                                    <Text style={styles.desc} numberOfLines = { 2 } >{project.description}</Text>
                                </TouchableOpacity>
                                <View style={styles.seperater}></View>
                                <View style={[styles.flexRow, styles.listFooter]}>
                                    <Text style={styles.label}>No. of Item: {project.items}</Text>
                                    <Text style={[styles.label, styles.primaryColor]}>{'EXPECTED DATE: '+project.expected_date}</Text>
                                </View>
                            </View>               
                        </View>
                    )
                })
            )
        } else {
            return (
                projectList.map((project, key) => {
                    return (
                        <View key={'closeProject-'+key} style={styles.card}>
                            <View>
                                <View style={[styles.flexRow, styles.listHeader]}>
                                    <Text style={styles.highlight}>ID #{project.id}</Text>
                                    <Text style={styles.label}>{'Stated on '+project.started_date}</Text>
                                </View>
                                <TouchableOpacity 
                                    style={styles.mv10}
                                    onPress={this.openProjectDetails.bind(this, project.id)}
                                >
                                    <Text style={styles.title} >{project.title}</Text>
                                    <Text style={styles.desc}  numberOfLines = { 2 } >{project.description}</Text>
                                </TouchableOpacity>
                                <View style={styles.seperater}></View>
                                <View style={[styles.flexRow, styles.listFooter]}>
                                    <Text style={styles.label}>No. of Item: {project.items}</Text>
                                    <Text style={[styles.label, styles.primaryColor]}>{'COMPLETED DATE: '+project.completed_date}</Text>
                                </View>
                            </View>               
                        </View>
                    )
                })
            )
        }
    }
}

const styles = StyleSheet.create({
    ...ApplicationStyles,
    projectCounts: {
        justifyContent: 'space-between',
    },
    flex05: {
        flex: 0.5
    },
    listHeader: {
        justifyContent: 'space-between',
    },
    listFooter: {
        justifyContent: 'space-between',
        paddingVertical: 10
    },
    highlight: {
        color: '#fff',
        paddingVertical: 5,
        paddingHorizontal: 15,
        backgroundColor: "#045e9f",
        fontSize: 12
    },
    title: {
        fontSize: 18,
    },
    desc: {
        fontSize: 11,
        color: "#ccc",
    }
});
