import React from 'react';
import { StyleSheet, Text, View, ScrollView, Image, TouchableOpacity, ActivityIndicator } from 'react-native';

import FixtureAPI from '../Services/FixtureAPI';
import ApplicationStyles from '../Theme/ApplicationStyles'
import API from '../Services/Api'

export default class ProjectDetailsScreen extends React.Component {

    constructor(props) {
        super(props)
        this.api = API.create()
        const { navigation } = this.props;
        this.projectId = navigation.getParam('pID', '');
        this.state = {
            data: null
        }
        this.onResponse = this.onResponse.bind(this)
    }

    static navigationOptions = ({navigation}) => {
        return {
            title: 'Project # ' + navigation.getParam('pID', '')
        }
    };

    componentDidMount() {
        let projectId = 1
        this.api.getProjectsByID
            .apply(this, [projectId])
            .then( (results) => {
                this.onResponse(results)
            }
        )
    }

    onResponse(response) {
        if(response.status == 200) {
            var responseData = response.data
            this.setState({data: responseData})
        }
    }

    render() {
        if(this.state.data == null) {
            return (
                <View style={styles.mainLoader}>
                    <ActivityIndicator
                        size={'large'}
                        color={"#045e9f"}
                        animating={true} />
                </View>
            )
        }
        else {
            return (
                <View style={styles.container}>
                    <View style={styles.wrapper}>
                        <FixedHeader data={this.state.data.projects_details} />
                        <ScrollView>
                            <View style={[styles.contentContainer]}>
                                <Content data={this.state.data.project_items} projectID={this.state.data.projects_details.id} navigation={this.props.navigation} />
                            </View>
                        </ScrollView>
                    </View>
                </View>
            )
        }
    }
}

class FixedHeader extends React.Component {
    render() {
        let projects_details = this.props.data
        return (
            <View style={[styles.pageHeader, styles.shadow ]}>
                <View style={[styles.flexRow, styles.headerRow1]}>
                    <Text style={styles.highlight}>ID #{projects_details.id}</Text>
                    <Text style={[styles.label, styles.primaryColor]}>{'Expected Date : '+projects_details.expected_date}</Text>
                </View>
                <View style={styles.mv10}>
                    <Text style={styles.title} >{projects_details.title}</Text>
                    <Text style={styles.desc} >{`Delivery Location : ${projects_details.location}`}</Text>
                </View>
                <View style={styles.seperater}></View>
                <View style={[styles.flexRow, styles.headFooter]}>
                    <View>
                        <Text style={styles.label}>CATEGORY</Text>
                        <Text style={[styles.textCenter]}>{projects_details.category}</Text>
                    </View>
                    <View>
                        <Text style={styles.label}>TYPE OF PIPE</Text>
                        <Text style={[styles.textCenter]}>{projects_details.type_of_pipe}</Text>
                    </View>
                    <View>
                        <Text style={styles.label}>QUANTITY WANTED</Text>
                        <Text style={[styles.textCenter]}>{projects_details.quantity}</Text>
                    </View>
                </View>
            </View>
        )
    }
}

class Content extends React.Component {
    
    openItemDetails(id) {
        this.props.navigation.navigate('ItemDetails', {
            itemID: id,
            projectID: this.props.projectID
        });
    }
    
    render() {
        let project_items = this.props.data
        return(
            project_items.map((project_item, key) => {
                return (
                    <View key={`item-${key}`} style={[styles.card ]}>
                        <TouchableOpacity 
                            style={[styles.flexRow, {alignItems: 'center'}]}
                            onPress={this.openItemDetails.bind(this, project_item.id)}
                        >
                            <View style={{flex:0.3}}>
                                <Image resizeMode={'cover'} style={{width: 100, height: 110}} source={require('../Images/pipeline.jpg')}/>
                            </View>
                            <View style={[styles.pl10, { flex:0.7}]}>
                                <View>
                                    <Text style={[styles.itemId]}>Item #{project_item.id}</Text>
                                </View>
                                <View style={[styles.mv10, styles.contentWrap]}>
                                    <Text style={styles.title} numberOfLines = { 1 } >{project_item.title}</Text>
                                </View>
                                <View style={[styles.flexRow, styles.listFooter]}>
                                    <View>
                                        <Text style={styles.label}>PRICE OF ITEM</Text>
                                        <Text style={[styles.textCenter]}>{project_item.price_of_item}</Text>
                                    </View>
                                    <View>
                                        <Text style={styles.label}>TOTAL PRICE OF ITEM</Text>
                                        <Text style={[styles.textCenter]}>{project_item.total_price}</Text>
                                    </View>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                )    
            })
        )
    }
}

const styles = StyleSheet.create({
    ...ApplicationStyles,
    pageHeader: {
        backgroundColor: '#fff',
        padding: 10,
        borderBottomWidth: 1,
        borderColor: "#ccc",
    },
    wrapper: {flex: 1},
    highlight: {
        color: '#fff',
        paddingVertical: 5,
        paddingHorizontal: 15,
        backgroundColor: "#045e9f",
        fontSize: 12
    },
    itemId: {
        alignSelf: 'flex-start',
        color: '#fff',
        paddingVertical: 5,
        paddingHorizontal: 10,
        backgroundColor: "#045e9f",
        fontSize: 12
    },
    headerRow1: {
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    title: {
        fontSize: 18,
    },
    desc: {
        fontSize: 11,
        color: "#ccc",
    },
    cardContent: {
        alignItems: 'center'
    },
    itemDetails: {
        // alignItems: 'flex-start',
    },
    headFooter: {
        justifyContent: 'space-between',
        paddingVertical: 10
    },
    listFooter: {
        justifyContent: 'space-between',
        paddingBottom: 10
    },
});